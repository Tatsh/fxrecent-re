#!/usr/bin/env python
from os.path import basename
from typing import BinaryIO, List, NamedTuple, Optional, Sequence, Tuple, Union
import logging
import struct
import sys

from typing_extensions import Literal


class FXRecentFolderHeader(NamedTuple):
    magic: Literal[b'book']
    filesize: int
    unk1: int
    unk2: int
    unk3: bytes  # 32
    unk4: int
    unk5: int
    unk6: int
    unk7: int


log: Optional[logging.Logger] = None


def setup_logging_stdout(name: Optional[str] = None,
                         verbose: bool = False) -> None:
    global log
    name = name if name else basename(sys.argv[0])
    log = logging.getLogger(name)
    log.setLevel(logging.DEBUG if verbose else logging.INFO)
    channel = logging.StreamHandler(sys.stdout)
    channel.setFormatter(logging.Formatter('%(message)s'))
    channel.setLevel(logging.DEBUG if verbose else logging.INFO)
    log.addHandler(channel)


def parse_book(
    f: BinaryIO
) -> Tuple[FXRecentFolderHeader, List[Union[bytes, int, str]]]:
    assert log is not None
    magic = f.read(4)
    log.debug('offset: 0x%x: %c%c%c%c', f.tell() - 4, *magic)
    header_data: List[Union[int, bytes, Literal[b'book']]] = [magic]
    assert magic == b'book'

    filesize = struct.unpack('<I', f.read(4))[0]
    header_data.append(filesize)
    log.debug('offset: 0x%x, file size: %d bytes', f.tell() - 4, filesize)

    # same between two files
    u: int = struct.unpack('>I', f.read(4))[0]
    header_data.append(u)
    log.debug('offset 0x%x: %d, 0x%x, %s', f.tell() - 4, u, u, bin(u))

    # same between two files
    u = struct.unpack('<I', f.read(4))[0]
    header_data.append(u)
    log.debug('offset 0x%x: %d, 0x%x, %s', f.tell() - 4, u, u, bin(u))

    # some kind of size?
    # different between two files
    header_data.append(f.read(48))
    u = struct.unpack('<I', f.read(4))[0]
    header_data.append(u)
    log.debug('offset 0x%x: %d, 0x%x, %s', f.tell() - 4, u, u, bin(u))

    # 4, same between two files
    u = struct.unpack('<I', f.read(4))[0]
    header_data.append(u)
    log.debug('offset 0x%x: %d, 0x%x, %s', f.tell() - 4, u, u, bin(u))

    # same between two files
    u = struct.unpack('<I', f.read(4))[0]
    header_data.append(u)
    log.debug('offset 0x%x: %d, 0x%x, %s', f.tell() - 4, u, u, bin(u))

    # same between two files
    u = struct.unpack('<I', f.read(4))[0]
    header_data.append(u)
    log.debug('offset 0x%x: %d, 0x%x, %s', f.tell() - 4, u, u, bin(u))

    rest: List[Union[bytes, int, str]] = []

    # path components without /
    # length, 0x01 0x01 0x00 0x00, string itself + padding with zeros
    while True:
        length: int = struct.unpack('<I', f.read(4))[0]
        log.debug('offset 0x%x: %d, 0x%x, %s',
                  f.tell() - 4, length, length, length)
        unk = f.read(4)
        log.debug('offset 0x%x: %s',
                  f.tell() - 4, ' '.join(hex(x) for x in unk))
        rest.append(length)
        rest.append(unk)

        # assume this is the end
        # 8 is the magic value of the next field?
        # unk = same between two files, 0x01 0x06 0x00 0x00
        if unk != b'\x01\x01' and length == 8 and (f.tell() % 4) == 0:
            break

        s = f.read(length).decode('utf-8')
        rest.append(s)
        log.debug('offset 0x%x: length = %d, "%s"',
                  f.tell() - length, length, s)
        if (f.tell() % 4) != 0:
            off = f.tell() + (4 - (f.tell() % 4))
            f.seek(off)

    # same between 2 files, 0x10 0x00 0x00 0x00
    u = struct.unpack('<I', f.read(4))[0]
    log.debug('offset 0x%x: %d, 0x%x, %s', f.tell() - 4, u, u, bin(u))
    rest.append(u)
    assert u == 16

    # different between two files
    u = struct.unpack('<I', f.read(4))[0]
    rest.append(u)
    log.debug('offset 0x%x: %d, 0x%x, %s', f.tell() - 4, u, u, bin(u))

    return FXRecentFolderHeader(*header_data), rest  # type: ignore


def main(argc: int, argv: Sequence[str]) -> int:
    setup_logging_stdout(verbose=True)
    for x in argv[1:]:
        with open(x, 'rb') as f:
            parse_book(f)
    return 0


if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
